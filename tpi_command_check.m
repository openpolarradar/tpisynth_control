checksum = hex2dec('0x00') + hex2dec('0x02') + hex2dec('0x08') + hex2dec('0x01')

checksum_bin = logical(dec2bin(checksum,8) - '0')
checksum_bin = ~checksum_bin

checksum_hex = dec2hex(bin2dec(char(checksum_bin + '0')),2)

% tpi_command_check
% checksum =
%     11
% checksum_bin =
%   1×8 logical array
%    0   0   0   0   1   0   1   1
% checksum_bin =
%   1×8 logical array
%    1   1   1   1   0   1   0   0
% checksum_hex =
%     'F4'


