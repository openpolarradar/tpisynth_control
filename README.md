# tpisynth_control

Linux project. Builds in Ubuntu 20 for 64bit x86. Likely will compile on many other platforms with minimal modification that are supported by FTDI D2XX.

Opens the FTDI USB device FTDI "FT245R USB FIFO" USB-to-serial/D2XX chip on the TPI Synthesizer V5.8 module (https://rf-consultant.com/) and sends bytes to control the SPI interface of the AD4351 chip in the module.

The FTDI D2XX interface is used in asynchronous bit bang mode.

	(assuming you have the d2xx library in the /usr/local/lib directory).
	cc main.c -o tpisynth_control -Wall -Wextra -lftd2xx -lpthread -lrt -Wl,-rpath /usr/local/lib -L/usr/local/lib

Example of running program:
```
cresis1@aq-field17:/arena/drivers_tpisynth_control/release/examples/write$ ./tpisynth_control AK05QGD8 1600 0
Opening FTDI device 0 (serial_num AK05QGD8) at 9600 baud, frequency 1600 MHz, power level 0.
  Succeeded
```

## tpisynth_control: install FTDI D2XX shared library and static library.

Download from https://ftdichip.com/drivers/d2xx-drivers/

For Ubuntu 2020, used:

Linux_FTDI_D2XX_drivers_libftd2xx-x86_64-1.4.27.tgz

NOTE:
* ubuntu20_libs/* built for Ubuntu 20 64bit x86; can be copied into /usr/local/lib/ so that Makefile works
* header files ftd2xx.h and WinTypes.h are from Linux_FTDI_D2XX_drivers_libftd2xx-x86_64-1.4.27.tgz:/release/examples/ directory
* main.c is based on Linux_FTDI_D2XX_drivers_libftd2xx-x86_64-1.4.27.tgz:/release/examples/write/main.c
* Makefile modified from Linux_FTDI_D2XX_drivers_libftd2xx-x86_64-1.4.27.tgz:/release/examples/write/Makefile and its dependencies
* Makefile modified to statically link libftd2xx.a so that FTDI libraries are not required to run
* main.c is also based on Remote Sensing Solutions arenaUserApps SubSystemTPISynth.cpp (May 21 2015)

Linux README.txt from FTDI:

```
1.  tar xfvz libftd2xx-arm-v8-1.4.18.tgz

This unpacks the archive, creating the following directory structure:

    build
        libftd2xx        (re-linkable objects)
        libusb           (re-linkable objects)
        libftd2xx.a      (static library)
        libftd2xx.so.1.4.18   (dynamic library)
        libftd2xx.txt    (platform-specific information)
    examples
    libusb               (source code)
    ftd2xx.h
    WinTypes.h

2.  cd build

3.  sudo -s 
  or, if sudo is not available on your system: 
    su

Promotes you to super-user, with installation privileges.  If you're
already root, then step 3 (and step 7) is not necessary.

4.  cp libftd2xx.* /usr/local/lib

Copies the libraries to a central location.

5.  chmod 0755 /usr/local/lib/libftd2xx.so.1.4.18

Allows non-root access to the shared object.

6.  ln -sf /usr/local/lib/libftd2xx.so.1.4.18 /usr/local/lib/libftd2xx.so

Creates a symbolic link to the 1.4.18 version of the shared object.

7.  exit
```

