/*
	tpisynth_control

	See README.md
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ftd2xx.h"

#define ARRAY_SIZE(x) sizeof((x))/sizeof((x)[0])

int main(int argc, char *argv[])
{
	int        retCode = -1; // Assume failure
	FT_STATUS  ftStatus = FT_OK;
	FT_HANDLE  ftHandle = NULL;
	int        portNum = -1; // Deliberately invalid
	DWORD      bytesWritten = 0;
	int        baudRate = 9600;

	char *serial_num;
	int frequency_MHz;
	int power_level;

	// ===========================================================================
	// Get list of all devices that are attached
	// ===========================================================================
	DWORD numDevs = 0;
	FT_DEVICE_LIST_INFO_NODE *devInfo = NULL;

	// create the device information list
	ftStatus = FT_CreateDeviceInfoList(&numDevs);
	if (ftStatus != FT_OK)
	{
		printf("FT_CreateDeviceInfoList failed, with error %d.\n", (int)ftStatus);
		goto exit;
	}

	if (numDevs == 0)
	{
		printf("No compatible USB FTDI devices attached or found.\n");
		goto exit;
	}

	if (numDevs > 0)
	{
		// allocate storage for list based on numDevs
		devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);
		// get the device information list
		ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);
		if (ftStatus != FT_OK)
		{
			printf("FT_GetDeviceInfoList failed, with error %d.\n", (int)ftStatus);
			goto exit;
		}

		// Print list of all devices that are attached
		for (unsigned int i = 0; i < numDevs; i++)
		{
			if (devInfo[i].ID == 0x0) {
				printf("There is a device with ID of 0 which indicates \"sudo rmmod ftdi_sio\" needs to be run.\n");
			}
		}
	}

	// ===========================================================================
	// Check input arguments
	// ===========================================================================
	if (argc != 4)
	{
		printf("Syntax: %s [serial_num] [frequency_MHz] [power_level]\n", argv[0]);
		printf("  serial_num: as indicated on the device e.g. A2001vyr, AK05QGD8\n");
		printf("  frequency_MHz: integer multiple of 10 between 40 and 4400\n");
		printf("  power_level: integer 0 to 3 (corresponding roughly to 8, 11, 14, and 17 dBm)\n");
		printf("\n");
		printf(" Example: tpisynth_control AK05QGD8 1600 0\n");
		printf("\n");
		printf("\"sudo rmmod ftdi_sio\" must be run before this function will work. Module ftdi_sio is the default driver module for the FTDI chip and must be removed to use the custom FTDI D2XX device.\n");
		printf("\n");

		// Print list of all devices that are attached
		for (unsigned int i = 0; i < numDevs; i++)
		{
			printf("Dev %d:\n",i);
			printf(" Flags: 0x%x\n",devInfo[i].Flags);
			printf(" Type: 0x%x\n",devInfo[i].Type);
			printf(" ID: 0x%x\n",devInfo[i].ID);
			if (devInfo[i].ID == 0x0) {
				printf("   ID of 0 indicates \"sudo rmmod ftdi_sio\" needs to be run.\n");
			}
			printf(" LocId: 0x%x\n",devInfo[i].LocId);
			printf(" SerialNumber: %s\n",devInfo[i].SerialNumber);
			printf(" Description: %s\n",devInfo[i].Description);
			printf(" ftHandle: 0x%p\n",devInfo[i].ftHandle);
			printf("\n");
		}

		goto exit;
	}
	else
	{
		serial_num = argv[1];
		sscanf(argv[2], "%d", &frequency_MHz);
		sscanf(argv[3], "%d", &power_level);

		for (unsigned int i = 0; i < numDevs; i++)
		{
			if (!strcmp(devInfo[i].SerialNumber, serial_num))
			{
				portNum = i;
				break;
			}
		}
		if (portNum == -1)
		{
			printf("Serial number %s not found in list of connected devices:\n", serial_num);
			printf("\n");

			// Print list of all devices that are attached
			for (unsigned int i = 0; i < numDevs; i++)
			{
				printf("Dev %d:\n",i);
				printf(" Flags=0x%x\n",devInfo[i].Flags);
				printf(" Type=0x%x\n",devInfo[i].Type);
				printf(" ID=0x%x\n",devInfo[i].ID);
				printf(" LocId=0x%x\n",devInfo[i].LocId);
				printf(" SerialNumber=%s\n",devInfo[i].SerialNumber);
				printf(" Description=%s\n",devInfo[i].Description);
				printf(" ftHandle=0x%p\n",devInfo[i].ftHandle);
				printf("\n");
			}

			goto exit;
		}

   		if (frequency_MHz < 40 || frequency_MHz > 4400 || frequency_MHz % 10 != 0)
		{
			printf("  frequency_MHz is %d, must be integer multiple of 10 between 40 and 4400\n", frequency_MHz);
			goto exit;
		}

   		if (power_level < 0 || power_level > 3)
		{
			printf("  power_level is %d, must be integer 0 to 3 (corresponding roughly to 8, 11, 14, and 17 dBm)\n", power_level);
			goto exit;
		}
	}
		
	printf("Opening FTDI device %d (serial_num %s) at %d baud, frequency %d MHz, power level %d.\n", portNum, serial_num, baudRate, frequency_MHz, power_level);
	
	ftStatus = FT_Open(portNum, &ftHandle);
	if (ftStatus != FT_OK) 
	{
		printf("FT_Open(%d) failed, with error %d.\n", portNum, (int)ftStatus);
		printf("Use lsmod to check if ftdi_sio (and usbserial) are present.\n");
		printf("If so, unload them using rmmod, as they conflict with ftd2xx.\n");
		goto exit;
	}

	assert(ftHandle != NULL);

	ftStatus = FT_ResetDevice(ftHandle);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_ResetDevice returned %d.\n", (int)ftStatus);
		goto exit;
	}
	
	ftStatus = FT_SetBaudRate(ftHandle, (ULONG)baudRate);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_SetBaudRate(%d) returned %d.\n", 
		       baudRate,
		       (int)ftStatus);
		goto exit;
	}
	
	ftStatus = FT_SetDataCharacteristics(ftHandle, 
	                                     FT_BITS_8,
	                                     FT_STOP_BITS_1,
	                                     FT_PARITY_NONE);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_SetDataCharacteristics returned %d.\n", (int)ftStatus);
		goto exit;
	}
	                          
	// Indicate our presence to remote computer
	ftStatus = FT_SetDtr(ftHandle);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_SetDtr returned %d.\n", (int)ftStatus);
		goto exit;
	}

	// Flow control is needed for higher baud rates
	ftStatus = FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0, 0);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_SetFlowControl returned %d.\n", (int)ftStatus);
		goto exit;
	}

	// Assert Request-To-Send to prepare remote computer
	ftStatus = FT_SetRts(ftHandle);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_SetRts returned %d.\n", (int)ftStatus);
		goto exit;
	}

	ftStatus = FT_SetTimeouts(ftHandle, 3000, 3000);	// 3 seconds
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_SetTimeouts returned %d\n", (int)ftStatus);
		goto exit;
	}


	unsigned int ad4351_reg[6];

	int m_bExtReference = 1;

	int vcoFreqInt;

	int divider = 0;
	while (1)
	{
		vcoFreqInt = frequency_MHz * (1 << divider);
		if (vcoFreqInt >= 2200) break;
		divider++;
	}

	// There are 6 SPI controlled registers
	// The bottom three bits of the 32 bit SPI writes, control the address: ad4351_reg[N] & 0x7
	if (frequency_MHz < 160)
	{
		ad4351_reg[5] = 0x00580005;
		ad4351_reg[4] = 0x00850024 | (divider << 20); // add divider
		ad4351_reg[4] |= (0x03 & power_level) << 3;
		ad4351_reg[3] = 0x000004b3;
		ad4351_reg[2] = 0x00004e42;
		ad4351_reg[1] = 0x08000011;
		ad4351_reg[0] = (vcoFreqInt / 10) << 15 ;
	 }
	 else
	 {
		ad4351_reg[5] = 0x00580005;
		ad4351_reg[4] = 0x00050024 | (divider << 20); // add divider
		ad4351_reg[4] |= (0x03 & power_level) << 3;
		ad4351_reg[3] = 0x000004b3;
		ad4351_reg[2] = 0x00004e42;
		ad4351_reg[1] = (divider) ? 0x00000011 : 0x08000011;
		ad4351_reg[0] = (frequency_MHz / 10) << 15 ;
	 }

	 //QByteArray dataArray;
	 char dataArray[2048];
	 unsigned int dataArrayIdx = 0;
	 unsigned char tempChar;

	 for (int regIndex = 5; regIndex >= 0; regIndex--) {
		for (int bitIndex = 31; bitIndex >= 0; bitIndex--) {
			tempChar = m_bExtReference ? 0x1 : 0x0; // External reference 0x01
			tempChar |= (ad4351_reg[regIndex] >> bitIndex & 0x01) ? 0x08 : 0x00; // Data pin 0x08
			dataArray[dataArrayIdx++] = tempChar;

			tempChar |= 0x04; // Clock pin 0x04
			dataArray[dataArrayIdx++] = tempChar;
		}

		tempChar |= 0x02; // End of 32-bit register write, latch enable to write 0x02
		dataArray[dataArrayIdx++] = tempChar;
	}

	// Effectively pauses for 8 clock cycles
	for (int bitIndex = 7; bitIndex >= 0; bitIndex--) {
		tempChar = m_bExtReference ? 0x1 : 0x0; // External reference 0x01
		// Note that "0x00 >> bitIndex" is always zero
		// So that (0x00 >> bitIndex & 0x01) is always zero
		// Implying that (0x00 >> bitIndex & 0x01) ? 0x08 : 0x00 is always zero
		tempChar |= (0x00 >> bitIndex & 0x01) ? 0x08 : 0x00; // Data pin 0x08
		dataArray[dataArrayIdx++] = tempChar;

		tempChar |= 0x04; // Clock pin 0x04
		dataArray[dataArrayIdx++] = tempChar;
	}

	tempChar |= 0x90; // External reference + data pins
	dataArray[dataArrayIdx++] = tempChar;

	tempChar = m_bExtReference ? 0x1 : 0x0; // External reference
	dataArray[dataArrayIdx++] = tempChar;

	// 0xFF: Set all bits to output
	// 0x1: Set bit mode to asynchronous bit bang mode
	ftStatus = FT_SetBitMode(ftHandle, 0xFF, 0x1);
	// Bytes written out to USB device will now be clocked out of the TPI FTDI part
	// at the specified BAUD rate. Each byte controls:
	// 0x08: SPI data bit
	// 0x04: SPI clock or enable bit
	// 0x02: SPI latch enable
	// 0x01: Reference clock control (1 is external reference)

	ftStatus = FT_SetBaudRate(ftHandle, 9600);

	ftStatus = FT_SetTimeouts(ftHandle, 3000, 3000);	// 3 seconds
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_SetTimeouts returned %d\n", (int)ftStatus);
		goto exit;
	}
	
	ftStatus = FT_Write(ftHandle, 
	                    dataArray,
	                    dataArrayIdx, 
	                    &bytesWritten);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_Write returned %d\n", (int)ftStatus);
		goto exit;
	}
	
	if (bytesWritten != dataArrayIdx)
	{
		printf("Failure.  FT_Write wrote %d bytes instead of %d.\n",
		       (int)bytesWritten,
		       (int)dataArrayIdx);
		goto exit;
	}

	printf("  Succeeded\n");
	retCode = 0;

exit:
	if (ftHandle != NULL)
		FT_Close(ftHandle);

	return retCode;
}
